var btnRun = document.querySelector('input[name="run"]');
var btnClear = document.querySelector('input[name="clear"]');

btnRun.addEventListener('click', clickRunBtn);
btnClear.addEventListener('click', clickClearBtn);

function clickRunBtn() {
    var x1 = document.getElementById('x1').value,
    x2 = document.getElementById('x2').value,
    result = document.getElementById('output'),
    inputs = document.querySelectorAll('label input'),
    addition = document.querySelector('#addition'),
    multiply = document.querySelector('#multiply'),
    prime = document.querySelector('#prime');

    clearOutput(result);

    if (x1 == null || x1 == "" || x2 == null || x2 == "") {
        addError(inputs);
        alert('One of the fields is empty!');

        return false;
    }

    x1 = parseInt(x1);
    x2 = parseInt(x2);

    if (Number.isNaN(x1) || Number.isNaN(x2)) {
        addError(inputs);
        alert('In the "X1" and "X2" fields should be entered numerical values!');

        return false;
    }

    if (x1 >= x2) {
        addError(inputs);
        alert('X2 must be greater than X1!');

        return false;
    }

    removeError(inputs);

    if (addition.checked) {
        output.innerHTML = `<p>Addition from ${x1} to ${x2}: <b>${addFromList(x1, x2)}</b></p>`;
    }
    if (multiply.checked) {
        output.innerHTML = `<p>Multiplying from ${x1} to ${x2}: <b>${multiplyFromList(x1, x2)}</b></p>`;
    }
    if (prime.checked) {
        output.innerHTML = `<p>Prime numbers from ${x1} to ${x2}: <b>${primeFromList(x1, x2)}</b></p>`;
    }
}

function clickClearBtn() {
    clearInput(document.querySelectorAll('input[type="text"]'));
}

function addFromList(start, finish) {
    var resultAdd = 0;
    for (var currentElem = start; currentElem <= finish; currentElem++) {
        resultAdd += currentElem;
    }

    return resultAdd;
}

function multiplyFromList(start, finish) {
    var resultMult = 1;
    for (var currentElem = start; currentElem <= finish; currentElem++) {
        resultMult *= currentElem;
    }

    return resultMult;
}

function primeFromList(start, finish) {
    var primeList = [];

    for (var currentElem = start; currentElem <= finish; currentElem++) {
        if (isPrimeNum(currentElem)) primeList.push(currentElem);
    }

    return primeList.join(', ');
}

function isPrimeNum(number) {

    if (number < 2) return false;

    for (var currentNum = 2; currentNum < number; currentNum++) {
        if (number % currentNum == 0) return false;
    }

    return true;
}

function addError(elementList) {
    for (let i = 0; i < elementList.length; i++) {
        elementList[i].classList.add('error');
    }
}

function removeError(elementList) {
    for (let i = 0; i < elementList.length; i++) {
        elementList[i].classList.remove('error');
    }
}

function clearOutput(element) {
    element.innerHTML = "";
}

function clearInput(elementList) {
    for (let i = 0; i < elementList.length; i++) {
        elementList[i].value = "";
    }
}
