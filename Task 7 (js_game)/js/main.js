(function main() {
    var gameWrapper 	= document.querySelector('.game-wrapper');			//GET GAME NODE
    var gameItemList 	= document.querySelectorAll('.game-item-back');		//GET LIST OF CARDS
    var triesAll 		= 0;												//COUNTER FOR EVERT TRY
    var triesRight 		= 0;												//COUNTER FOR SUCCESSFUL TRY
    var score 			= document.querySelector('.score');					//GET SCORE NODE
    var button 			= document.querySelector('.restart');				//GET RESTART BUTTON NODE

    var colorList 		= [
		"#FF9FF3", "#FF9FF3", "#FECA57", "#FECA57", 
		"#FF6B6B", "#FF6B6B", "#48DBFB", "#48DBFB", 
		"#1CD1A1", "#1CD1A1", "#2E86DE", "#2E86DE", 
		"#5F27CD", "#5F27CD", "#D8334A", "#D8334A" 
    ];


    var shuffledColors = colorList.sort(function() {						//SHUFFLE COLORS IN RANDOM ORDER
        return .5 - Math.random();
    });

																			//APPLYING RANDOM COLORS TO EVERY CARD
    for (let currentIndex = 0; currentIndex < gameItemList.length; currentIndex++) {
        gameItemList[currentIndex].style.backgroundColor = shuffledColors[currentIndex];
    }

    gameWrapper.addEventListener('click', playMemoryGame);					//GAME NODE ONCLICK
    button.addEventListener('click', restartGame);							//RESTART BUTTON ONCLICK


    function playMemoryGame(event) {										//DELEGATION OF CLICK INSIDE GAME NODE

        var target = event.target;

        if (!(target.classList.contains('game-item-front'))) return;			//IF CLICK ISN'T ON A CARD - BREAK

        var openedList = document.querySelectorAll('.flip');					//GET ALL FLIPPED CARDS

        if (openedList.length >= 2) return;										//IF FLIPPED CARDS >= 2 - BREAK (NO CLICKS AVAILABLE)

        if (openedList.length == 1) {											//IF ONE CARD ALREADY FLIPPED
            triesAll++;																//Plus one try
            target.parentNode.parentNode.classList.toggle('flip');					//Flip the second card
            openedList = document.querySelectorAll('.flip');						//Get list of 2 flipped cards nodes

            if (isSameColors(openedList)) {											//IF 2 CARDS HAVE THE SAME COLOR
                setTimeout(function() {													//Set timeout on 1 second
                    removeGameBlock(openedList);										//Remove guessed cards
                    triesRight++;														//Plus one right try

                    if (triesRight == 8) {												//IF ALL CARDS ARE GUESSED
                        score.innerHTML = `SCORE: ${Math.round(triesRight*100/triesAll)}/100!`;		//Show the result (per cent of right tries from all)
                        score.style.zIndex = '9999';
                    }
                }, 1000);
            }
            setTimeout(function() {													//Set timeout on 1 second
                removeClass(openedList, 'flip');									//"Unflip" cards
            }, 1000);
            return;
        }

        target.parentNode.parentNode.classList.toggle('flip');					//Flip cards
    }


    function removeGameBlock(array) {										//REMOVE GUESSED CARDS
        for (let i = 0; i < array.length; i++) {
            array[i].style.zIndex = '-100';
        }
    }


    function isSameColors(array) {											//CHECK IF 2 CARDS HAVE THE SAME COLOR
        return !!(array[0].querySelector('.game-item-back').style.backgroundColor == array[1].querySelector('.game-item-back').style.backgroundColor);
    }


    function removeClass(array, className) {								//"UNFLIP" CARDS
        [].forEach.call(array, function(elem) {
            elem.classList.remove(className);
        });
    }


    function restartGame() {												//RELOAD GAME
        location.reload();
    }

})();
